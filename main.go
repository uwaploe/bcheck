// Battery monitor for the Deep Profiler DPC
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strconv"
	"time"

	smartbat "bitbucket.org/mfkenney/go-smartbat"
	"github.com/garyburd/redigo/redis"
	"github.com/ugorji/go/codec"
	"gopkg.in/yaml.v2"
)

// Default battery configuration
var defcfg = `
addrs:
  - 0x7000
  - 0x7001
  - 0x7002
  - 0x7003
  - 0x7004
  - 0x7005
  - 0x7700
  - 0x7701
  - 0x7702
  - 0x7703
variables:
  - s/n
  - voltage
  - current
  - max_error
  - energy
  - cycles
  - status
  - temperature
  - rel_charge
  - avg_current
  - chg_current
summary:
  - name: voltage
    avg: !!bool true
    scale: 1000
  - name: current
    avg: !!bool false
    scale: 1000
  - name: energy
    avg: !!bool false
    scale: 1000
  - name: rel_charge
    avg: !!bool true
    scale: 1
  - name: temperature
    avg: !!bool true
    scale: 1000
  - name: avg_current
    avg: !!bool false
    scale: 1000
dbkey: "battery_status"
pubchan: "data.power"
`

var Version = "dev"
var BuildDate = "unknown"

type SummaryVar struct {
	Name  string
	Avg   bool
	Scale int
}

type BatteryCfg struct {
	Addrs     []uint16
	Variables []string
	Summary   []SummaryVar
	Dbkey     string
	Pubchan   string
}

// Calculate the battery data summary
func summary(records []smartbat.Record,
	avgvars, sumvars []string) map[string]float32 {

	output := make(map[string]float32)
	names := append(avgvars, sumvars...)
	for _, rec := range records {
		for _, name := range names {
			switch t := rec.Data[name].(type) {
			case float32:
				output[name] += t
			case uint16:
				output[name] += float32(t)
			}
		}
	}

	n := float32(len(records))
	for _, name := range avgvars {
		output[name] = output[name] / n
	}

	return output
}

// Publish battery records to a Redis channel
func publish(conn redis.Conn, channel string, ts time.Time,
	records []smartbat.Record) {
	var buf []byte
	var mh codec.MsgpackHandle

	v := make([]interface{}, 3)

	v[0] = int32(ts.Unix())
	v[1] = int32(ts.Nanosecond() / 1000)

	data := make([]map[string]interface{}, len(records))
	for i, rec := range records {
		data[i] = rec.Data
	}
	v[2] = data

	enc := codec.NewEncoderBytes(&buf, &mh)
	err := enc.Encode(v)
	if err != nil {
		log.Printf("Encoding error: %v", err)
	} else {
		conn.Do("PUBLISH", channel, buf)
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] [cfgfile]\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Sample DP SmartBatteries and write to Redis\n\n")
		flag.PrintDefaults()
	}

	dumpcfg := flag.Bool("dumpcfg", false, "Dump default config to stdout")
	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	server := flag.String("server", "localhost:6379",
		"network address (HOST:PORT) of Redis server")

	flag.Parse()
	args := flag.Args()

	if *showvers {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Fprintf(os.Stdout, "---\n%s\n", defcfg)
		os.Exit(0)
	}

	var contents []byte
	var err error

	if len(args) >= 1 {
		contents, err = ioutil.ReadFile(args[0])
		if err != nil {
			log.Fatalf("Error reading configuration: %v", err)
		}
	} else {
		contents = []byte(defcfg)
	}

	cfg := BatteryCfg{}
	err = yaml.Unmarshal(contents, &cfg)
	if err != nil {
		log.Fatalf("Bad configuration format: %v", err)
	}

	rdrs := make([]*smartbat.Reader, 0)
	for _, s := range cfg.Addrs {
		b := smartbat.NewReader(0, s, cfg.Variables)
		if b != nil {
			rdrs = append(rdrs, b)
		}
	}

	if len(rdrs) == 0 {
		log.Fatalf("No batteries found")
	}

	sumvars := make([]string, 0)
	avgvars := make([]string, 0)
	scale := make(map[string]float32)
	for _, sv := range cfg.Summary {
		scale[sv.Name] = float32(sv.Scale)
		if sv.Avg {
			avgvars = append(avgvars, sv.Name)
		} else {
			sumvars = append(sumvars, sv.Name)
		}
	}

	records := make([]smartbat.Record, 0)
	tstamp := time.Now()
	for _, rdr := range rdrs {
		rec, err := rdr.Read()
		if err != nil {
			log.Println(err)
			continue
		}
		records = append(records, rec)
	}
	status := summary(records, avgvars, sumvars)

	conn, err := redis.Dial("tcp", *server)
	if err == nil {
		conn.Send("MULTI")
		conn.Send("HSET", cfg.Dbkey, "t", strconv.FormatInt(tstamp.Unix(), 10))
		conn.Send("HSET", cfg.Dbkey, "count", strconv.Itoa(len(records)))
		for k, v := range status {
			sc := scale[k]
			if k == "temperature" {
				k = "temp"
			}
			if sc > 0 {
				conn.Send("HSET", cfg.Dbkey, k,
					strconv.FormatInt(int64(v*sc), 10))
			} else {
				conn.Send("HSET", cfg.Dbkey, k,
					strconv.FormatFloat(float64(v), 'f', 2, 32))
			}
		}
		_, err = conn.Do("EXEC")
		if err != nil {
			log.Println(err)
		}
		publish(conn, cfg.Pubchan, tstamp, records)
	} else {
		log.Printf("Summary of %d batteries", len(records))
		log.Printf("%v\n", status)
	}
}
