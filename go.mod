module bitbucket.org/uwaploe/bcheck

go 1.13

require (
	bitbucket.org/mfkenney/go-smartbat v1.2.0
	github.com/garyburd/redigo v1.6.0
	github.com/ugorji/go/codec v1.1.7
	gopkg.in/yaml.v2 v2.3.0
)
