# DPC Battery Status Check

This application checks the status of all batteries on the Deep Profiler
Controller and writes the information to the [Redis](http://redis.io)
data-store. It is designed to run as a "cron job" on the DPC.

## Output

A summary of the battery status is written to the Redis hash table
*battery_status*. Data from this table is incorporated into the status
messages sent by the DPC over the inductive modem link.

The `redis-cli` program can be used to view the latest values:

``` shellsession
$ redis-cli hgetall battery_status
 1) "t"
 2) "1489604882"
 3) "count"
 4) "8"
 5) "temp"
 6) "25630"
 7) "voltage"
 8) "13610"
 9) "current"
10) "0"
11) "energy"
12) "567000"
13) "rel_charge"
14) "51"
15) "avg_current"
16) "0"
$
```

| **Key**               | **Description**                                     |
|--------------------- |--------------------------------------------------- |
| t                     | Timestamp ins seconds since 1/1/1970 UTC            |
| count                 | Number of batteries sampled                         |
| temp                  | Average battery temperature in millidegrees C            |
| voltage               | Average battery voltage in millivolts                    |
| current               | Total instantaneous current in milliamps                 |
| energy                | Total remaining capacity in milliwatt-hours              |
| rel\_charge  | Remaining capacity as a percentage                  |
| avg\_current | Total current averaged over the past minute in milliamps |
